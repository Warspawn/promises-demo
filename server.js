// promises demo server

var express = require('express');
var app = express();
var q = require('q');
var _ = require('underscore');

// entity
var Student = require('./student'),
    ClassRoster = require('./roster'),
    Assignment = require('./assignment');

var genStudents = function() {
    var names = [
        "Marya Stumbaugh",
        "Dorcas Stinebaugh",
        "Lavelle Ketchum",
        "Thaddeus Amero",
        "Fleta Mckeever ",
        "Anjelica Sellman",
        "Clement Brannigan",
        "Shaniqua Westerfield",
        "Katie Trueblood",
        "Cleora Westgate",
        "Estefana Leverett",
        "Randa Gulley",
        "Noel Hilson",
        "Margarette Pinette",
        "Sara Ritacco",
        "Sindy Cumming",
        "Amina Orrell",
        "Basilia Hosler",
        "Angelica Krebs",
        "Drusilla Petrillo"
    ];

    var students = [];

    for (var x = 0; x < names.length; x++) {
        var student = new Student((x + 1), names[x]);
        students.push(student.$save());
    }

    return q.all(students);
};

// must have classes and students ready for assignments
var genAssignments = function(classes) {
    var assignments = [];

    for (var i = 0; i < 10; i++) {
        var a = new Assignment((i + 1), 'Lesson ' + (i + 1));
        a.grades = [];
        classes.forEach(function(roster) {
            _.each(roster.students, function(student) {
                a.grades.push({
                    classId: roster.id,
                    studentId: student,
                    score: _.random(0, 100)
                });
            });
        });
        assignments.push(a.$save());
    }

    return q.all(assignments);
};

// must create classes after students exist
var genClasses = function(students) {
    students = _.shuffle(_.pluck(students, 'id'));

    var class1 = new ClassRoster(1, 'Biology 101', _.first(students, 5));
    students = _.without(students, _.first(students, 5));
    var class2 = new ClassRoster(2, 'Chemistry For Housewives', _.first(students, 5));
    students = _.without(students, _.first(students, 5));
    var class3 = new ClassRoster(3, '101 Uses For Bacon', _.first(students, 5));

    return q.all([class1.$save(), class2.$save(), class3.$save()]);
};

var initData = function() {
    var process = genStudents()
    .then(genClasses)
    .then(genAssignments);

    return process;
};

app.use('/', express.static(__dirname + '/public'));

app.use(app.router);

app.get('/api/init', function(req, res) {
    initData().then(function(results) {
        res.send("OK");
    });
});

app.get('/api/classes/:classId', function(req, res) {
    var classId = req.params.classId;

    ClassRoster.get(classId)
        .then(function(roster) {
            res.send(roster);
        }, function(err) {
            res.send(500, err);
        });
});

app.get('/api/classes/:classId/students', function(req, res) {
    var classId = req.params.classId;

    ClassRoster.get(classId)
        .then(function(roster) {
            Student.getAll(roster.students).then(function(students) {
                res.send(students);
            }, function(err) {
                res.send(500, err);
            });
        }, function(err) {
            res.send(500, err);
        });
});

app.get('/api/classes/:classId/assignments', function(req, res) {
    var classId = req.params.classId;

    Assignment.getAll()
        .then(function(assignments) {
            //console.log('assignments: ', assignments);
            var results = _.filter(assignments, function(a) {
                return _.where(a.grades, {classId: parseInt(classId, 10)}).length > 0;
            });

            // strip grade data to be class specific
            _.each(results, function(assignment) {
                assignment.grades = _.where(assignment.grades, {classId: parseInt(classId, 10)});
            });

            //console.log("results: ", results);

            res.send(results || []);
        }, function(err) {
            res.send(500, err);
        });
});

app.post('/api/classes/:classId/students/:studentId', function(req, res) {
    var classId = req.params.classId,
        studentId = req.params.studentId;

    ClassRoster.get(classId).then(function(roster) {
        roster.$addStudent(studentId).then(function() {
            res.send(roster);
        });
    }, function(err) {
        res.send(500, err);
    });

});

app.get('/api/students/:studentId', function(req, res) {
    var studentId = req.params.studentId;

    Student.get(studentId)
        .then(function(student) {
            res.send(student);
        }, function(err) {
            res.send(500, err);
        });
});

app.get('/api/assignments/:assignmentId', function(req, res) {
    var assignmentId = req.params.assignmentId;

    Assignment.get(assignmentId)
        .then(function(assignment) {
            res.send(assignment);
        }, function(err) {
            res.send(500, err);
        });
});

app.listen(3333);

console.log('server running on http://localhost:3333');