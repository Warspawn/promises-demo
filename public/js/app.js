angular.module('PromiseDemo', [])
.controller('MainCtrl', ['$scope', '$log', 'ClassRoster', 'Assignment', function($scope, $log, ClassRoster, Assignment) {
    ClassRoster.get(1)
        .then(function(roster) {
            $scope.roster = roster;
            Assignment.getAllForRoster(roster.id)
                .then(function(assignments) {
                    $scope.roster.assignments = assignments;
                }, function(err) {
                    $log.error('error getting assignments!', err);
                });
        }, function(err) {
            $log.error('error getting roster!', err);
        });
}])
.factory('ClassRoster', ['$http', '$q', 'Student', function($http, $q, Student) {
    var Roster = function(json) {
        angular.copy(json || {}, this);
    };

    Roster.get = function(id) {
        // can run details in parrallel
        var p1 = $http.get('/api/classes/' + id)
            .then(function(response) {
                return new Roster(response.data);
            }, function(response) {
                return $q.reject(response.data);
            });

        var p2 = $http.get('/api/classes/' + id + '/students')
            .then(function(response) {
                var results = [];
                angular.forEach(response.data, function(student) {
                    results.push(new Student(student));
                });

                return results;
            }, function(err) {
                return $q.reject(err.data);
            });

        return $q.all([p1, p2]).then(function(results) {
            var roster = results[0],
                students = results[1];

            roster.students = students;
            return roster;
        }, function(err) {
            return $q.reject(err);
        });
    };

    return Roster;
}])
.factory('Student', ['$http', '$q', function($http, $q) {
    var Student = function(json) {
        angular.copy(json || {}, this);
    };

    Student.get = function(id) {
        return $http.get('/api/students/' + id)
            .then(function(response) {
                return new Student(response.data);
            }, function(response) {
                return $q.reject(response.data);
            });
    };

    return Student;
}])
.factory('Assignment', ['$http', '$q', function($http, $q) {
    var Assignment = function(json) {
        angular.copy(json || {}, this);
    };

    Assignment.prototype.$getScore = function(studentId) {
        var grade = _.find(this.grades, function(grade) {
            return grade.studentId === studentId;
        });

        return grade.score || 'NA';
    };

    Assignment.get = function(id) {
        return $http.get('/api/assignments/' + id)
            .then(function(response) {
                return new Assignment(response.data);
            }, function(response) {
                return $q.reject(response.data);
            });
    };

    Assignment.getAllForRoster = function(classId) {
        return $http.get('/api/classes/' + classId + '/assignments')
            .then(function(response) {
                var assignments = [];

                angular.forEach(response.data, function(data) {
                    assignments.push(new Assignment(data));
                });

                return assignments;
            }, function(response) {
                return $q.reject(response.data);
            });
    };

    return Assignment;
}]);