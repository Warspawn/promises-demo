// class roster entity
var fs = require('fs'),
    q = require('q');

var Roster = function(id, name, students) {
    if (id) {
        this.id = id;
    } else {
        this.id = 'NA';
    }

    if (name) {
        this.name = name;
    } else {
        this.name = "";
    }

    if (students) {
        this.students = students;
    } else {
        this.students = [];
    }
};

Roster.cache = {};

Roster.prototype.$addStudent = function(studentId) {
    this.students.push(studentId);
};

Roster.prototype.$save = function() {
    var roster = this;

    return q.nfcall(fs.writeFile, __dirname + '/data/roster-' + this.id + '.json', JSON.stringify(this, true, 4))
        .then(function() {
            // pass the data back through
            Roster.cache[roster.id] = roster;
            return roster;
        });
};

Roster.get = function(rosterId) {
    var deferred = q.defer(),
        filename = __dirname + '/data/roster-' + rosterId + '.json';

    if (Roster.cache[rosterId]) {
        deferred.resolve(Roster.cache[rosterId]);
    } else {
        try {
            var data = require(filename);
            deferred.resolve(data);
        } catch (ex) {
            deferred.reject('file not found: ' + filename);
        }
    }

    return deferred.promise;
};

module.exports = Roster;