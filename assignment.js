// assignment entity

var fs = require('fs'),
    q = require('q');

var Assignment = function(id, name) {
    if (id) {
        this.id = id;
    }

    if (name) {
        this.name = name;
    }
};

Assignment.prototype.$save = function() {
    var assignment = this;

    return q.nfcall(fs.writeFile, __dirname + '/data/assignment-' + this.id + '.json', JSON.stringify(this, true, 4))
        .then(function() {
            // pass the data back through
            return assignment;
        });
};

Assignment.get = function(assignmentId) {
    var deferred = q.defer();

    try {
        var data = require(__dirname + '/data/assignment-' + assignmentId + '.json');
        deferred.resolve(data);
    } catch (ex) {
        deferred.reject('file not found');
    }

    return deferred.promise;
};

Assignment.getAll = function() {
    var deferred = q.defer(),
        assignments = [];

    return q.nfcall(fs.readdir, __dirname + '/data')
        .then(function(files) {
            files.forEach(function(filename) {
                if (filename.search('assignment-') >= 0) {
                    try {
                        var data = require(__dirname + '/data/' + filename);
                        assignments.push(data);
                    } catch (e) {
                        console.log('error with file: ' + __dirname + '/data/' + filename + ' :: ' + e.message);
                    }
                }
            });

            return assignments;
        }, function(err) {
            return q.reject(err);
        });
};

module.exports = Assignment;