// student entity

var fs = require('fs'),
    q = require('q');

var Student = function(id, name) {
    if (id) {
        this.id = id;
    }

    if (name) {
        this.name = name;
    }
};

Student.cache = {};

Student.prototype.$save = function() {
    var student = this;

    return q.nfcall(fs.writeFile, __dirname + '/data/student-' + this.id + '.json', JSON.stringify(this, true, 4))
        .then(function() {
            // update cache
            Student.cache[student.id] = student;
            // pass the data back through
            return student;
        }, function(err) {
            console.log('failed to save student ', student);
            return $q.reject(err);
        });
};

Student.get = function(studentId) {
    var deferred = q.defer();

    if (Student.cache[studentId]) {
        deferred.resolve(Student.cache[studentId]);
    } else {
        try {
            var data = require(__dirname + '/data/student-' + studentId + '.json');
            deferred.resolve(data);
        } catch (ex) {
            deferred.reject('file not found');
        }
    }

    return deferred.promise;
};

Student.getAll = function(include) {
    var deferred = q.defer(),
        students = [],
        keys = Object.keys(Student.cache);

    if (keys.length > 0) {
        console.log('student cache:', Student.cache);
        keys.forEach(function(key) {
            if(!include) {
                students.push(Student.cache[key]);
            } else if(include && include.indexOf(parseInt(key, 10)) >= 0) {
                students.push(Student.cache[key]);
            }
        });
        deferred.resolve(students);
    } else {
        q.nfcall(fs.readdir, __dirname + '/data')
            .then(function(files) {
                //console.log('student files: ', files);
                files.forEach(function(filename) {
                    if (filename.search('student-') >= 0) {
                        try {
                            var data = require(__dirname + '/data/' + filename);

                            if(!include) {
                                students.push(data);
                            } else if(include && include.indexOf(data.id) >= 0) {
                                students.push(data);
                            }
                            Student.cache[data.id] = data;
                        } catch(e) {
                            return deferred.reject('error with file: ' + __dirname + '/data/' + filename + ' :: ' + e.message);
                        }
                    }
                });
                //console.log('student cache after files:', Student.cache);

                deferred.resolve(students);
            }, function(err) {
                return deferred.reject(err);
            });
    }

    return deferred.promise;
};

module.exports = Student;